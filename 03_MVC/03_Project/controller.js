import {model} from "./model.js";
import {EmployeeView, ProjectView} from "./view.js";

class Controller {
    constructor(){
        this.projectView = new ProjectView();
        this.employeeView = new EmployeeView();
        model.subscribe("error",this.projectView,this.projectView.printError);
        model.subscribe("addProject",this.projectView,this.projectView.addProject);
        model.subscribe("updateEmployee",this.employeeView,this.employeeView.updateEmployee);
    }

    init(){
        document.querySelector("#exampleModal button.btn-primary").onclick = (ev)=>{
            console.log("click");
            let title = document.querySelector("#title").value;
            let budget = document.querySelector("#budget").value;
            let desc = document.querySelector("#desc").value;
            //let employees = document.querySelector("#employees").value;
            let employees = document.querySelectorAll("#employees option:checked");
            let persons = Array.from(employees).map((el)=>el.value);
            console.log(persons);
            model.addProject(title, budget, desc, persons);
        }
    }
}

export const controller = new Controller();