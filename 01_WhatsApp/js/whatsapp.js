import Person from './person.js';
import Group from './group.js';
import Message from './message.js';

export default class WhatsApp {

    #ownId;
    #contactList
    #currentChatPartner

    constructor(){
        this.#contactList= new Map();
        this.#ownId=0;
        this.#currentChatPartner = undefined;
    }

    init(){
        this.#loadFromJSON();
        this.#initUI();
        this.#addEventHandler();
    }

    insertMessage(text,senderId,receiverId){
        let currentDate = new Date();
        let options = {
            hour: '2-digit', minute: '2-digit'
        }
        let time = currentDate.toLocaleTimeString('de-DE',options);
        let receiverContact = this.#contactList.get(receiverId);
        let obj = {
            text: text,
            time: time,
            senderId: senderId
        }
        let msg = new Message(obj,receiverContact instanceof Group);
        receiverContact.addMessage(msg);
        if(this.#currentChatPartner?.id == receiverId){
            msg.print(this.#ownId,document.querySelector(".chatroom"),this.#contactList);
        } else {
            receiverContact.contactDiv.classList.add("new-message");
        }
    }

    //================ PRIVATE =================
    #addEventHandler(){
        document.querySelector("ul.list-group").addEventListener("click",(e) => {
            let element = e.target.closest(".whatsapp-element");
            if(element){
                //add active class
                let elements = document.
                    querySelectorAll(".whatsapp-element");
                for(let el of elements){
                    el.classList.remove("active");
                }
                element.classList.add("active");
                element.classList.remove("new-message");
                document.querySelector(".chatroom").replaceChildren();
                //get the id from the element
                let id = element.id.split("_")[1];
                this.#currentChatPartner = this.#contactList.get(Number(id));
                this.#currentChatPartner.printHeader();
                this.#currentChatPartner.printMessages(this.#ownId,
                    document.querySelector(".chatroom"),this.#contactList);
                document.querySelector("#inputMsg").disabled=false;
            }
        });

        document.querySelector("#inputMsg").onkeyup=(ev)=>{
            if(ev.which == 13){
                this.insertMessage(ev.currentTarget.value,this.#ownId,this.#currentChatPartner.id);
            }
        };
    }

    #initUI(){
        document.querySelector("#inputMsg").disabled=true;
        document.querySelector("#headerImg").replaceChildren();
        document.querySelector(".list-group.border-top").replaceChildren();
        document.querySelector(".chatroom").replaceChildren();
    }

    #loadFromJSON(){
        fetch('json/contacts.json')
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP-Fehler! Status: ${response.status}`);
                }
                return response.json(); // Konvertiert die Antwort in JSON
            })
            .then(data => {
                console.log(data); // Die geladene JSON-Datei ausgeben
                this.#ownId = data.userId;
                for(let person of data.persons){
                    let p = new Person(person);
                    this.#contactList.set(p.id,p);
                    this.#addMessageToContact(p,person,false);
                }
                this.#printContactList();
            }).catch(error => {
                console.error('Fehler beim Laden der JSON-Datei:', error);
            });
    }

    #printContactList(){
        for(let contact of this.#contactList.values()){
            contact.printContact();
        }
    }

    #addMessageToContact(contact,jsonContact,isGroupMsg){
        for(let msg of jsonContact.messages){
            let message = new Message(msg,isGroupMsg);
            contact.addMessage(message);
        }
    }
}

