export class ProjectView {
    printError(error){
    }

    addProject(project){
        console.log(project);
        let html = `
        <div class="accordion" id="project_${project.id}">
          <div class="accordion-item">
            <h2 class="accordion-header">
              <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                ${project.title}
              </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                <strong>Budget: </strong> ${project.budget} <br>
                <strong>Description: </strong> ${project.desc} <br>
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header">
              <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Mitarbeiter
              </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
              <div class="accordion-body">
                ${this.#printEmployees(project.employees)}
              </div>
            </div>
          </div>
        </div>
        `
        document.querySelector("#projectList").
        insertAdjacentHTML("beforeend",html);

    }

    #printEmployees(employees){
        let html = "";
        for(let e of employees){
            html += `<strong>${e.firstName} ${e.lastName}</strong> <br>`;
        }
        return html
    }

    deleteProject(project){
    }
}

export class EmployeeView {
    updateEmployee(employee){
        console.log(employee);
    }
}



