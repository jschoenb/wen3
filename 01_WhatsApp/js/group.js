import Contact from "./contact.js";

export default class Group extends Contact {
    #contacts;

    constructor({id,name,img}){
        super({id,name,img});
        this.#contacts=[];
    }

    addContact(person){
        this.#contacts.push(person);
    }

    doPrintHeader(){
        let names = "";
        for(let i=0; i<this.#contacts.length;i++){
            names += this.#contacts[i].name;
            if (i < this.#contacts.length - 1) {
                names  += ", ";
            }
        }
        return `<span>${names}</span>`;
    }

}
