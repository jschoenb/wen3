export default class AnalogClock{
    constructor() {
        this.DOM = {
            hourHand: document.querySelector('[data-hour-hand]'),
            minuteHand: document.querySelector('[data-minute-hand]'),
            secondHand: document.querySelector('[data-second-hand]')
        }
    }

    setClock(currentDate){
        const secondsRatio = currentDate.getSeconds() / 60
        const minutesRatio = (secondsRatio + currentDate.getMinutes()) / 60
        const hoursRatio = (minutesRatio + currentDate.getHours()) / 12
        this.#setRotation(this.DOM.secondHand, secondsRatio)
        this.#setRotation(this.DOM.minuteHand, minutesRatio)
        this.#setRotation(this.DOM.hourHand, hoursRatio)
    }

    #setRotation(element,rotationRatio){
        element.style.setProperty('--rotation', rotationRatio * 360)
    }
}
