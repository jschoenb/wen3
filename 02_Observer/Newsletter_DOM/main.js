import Newsletter from "./newsletter.js";
import Person from "./person.js";

window.onload = function (){
    let newsletter = new Newsletter();

    let div = document.querySelector("#eventDiv");
    div.addEventListener("sendLetter", function(e){
       console.log(e);
       this.innerHTML += `<p>${e.detail.message}</p>`;
    });

    let p1 = new Person("Hannes", "Schönböck");
    p1.addEventListener("sendLetter", function (e){
        alert("Geht das????");
    });


    newsletter.send("Erste Nachricht vom KWM Newsletter", div);
    newsletter.send("Zweite Nachricht vom KWM Newsletter", div);
}