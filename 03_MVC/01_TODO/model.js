import Subject from "./subject.js";

class TodoModel extends Subject {
    #todos;

    static id=0;

    constructor() {
        super();
        this.#todos = new Map();
    }

    get todos() {
        return this.#todos;
    }

    add(title, description){
        if(title && description) {
            let task = {
                title: title,
                description: description,
                complete: false,
                id: ++TodoModel.id
            }
            this.#todos.set(task.id, task);
            console.info("Task added", task);
            this.notify("addTask", task);
        }
    }

    delete(id){
        let task = this.#todos.get(Number(id));
        if(task){
            this.#todos.delete(Number(id));
            super.notify("deleteTask", task);
        }
    }

    check(id,isComplete){
        let task = this.#todos.get(Number(id));
        if(task){
            task.complete = isComplete;
            this.#todos.set(Number(task),task);
            super.notify("updateTask", task);
        }
    }
}

export const todoModelInstance = new TodoModel();