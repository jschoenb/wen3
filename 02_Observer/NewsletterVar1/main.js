import Newsletter from "./newsletter.js";
import Person from "./person.js";

window.onload = function (){
    let newsletter = new Newsletter();

    let div = document.querySelector("#eventDiv");
    newsletter.subscribe(div, function(param){
       console.log(param);
       this.innerHTML += `<p>${param.message}</p>`;
    });

    let p1 = new Person("Hannes", "Schönböck");
    newsletter.subscribe(p1,p1.receivedMessage);


    newsletter.send("Erste Nachricht vom KWM Newsletter");

    newsletter.unsubscribe(p1);
    newsletter.send("Zweite Nachricht vom KWM Newsletter");
}