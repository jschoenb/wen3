import Subject from "./subject.js";

export default class Newsletter extends Subject{
    send(event,text) {
        this.notify(event,{message:text});
    }
}