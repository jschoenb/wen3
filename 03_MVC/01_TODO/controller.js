import {viewInstance} from "./TodoView.js";
import {todoModelInstance} from "./model.js";

class TodoController {
    init(){
        let dom = viewInstance.dom; //get the DOM elements
        //subscribe to the model
        todoModelInstance.subscribe("addTask",viewInstance,viewInstance.addTask);
        todoModelInstance.subscribe("deleteTask",viewInstance,viewInstance.deleteTask);
        todoModelInstance.subscribe("updateTask",viewInstance,viewInstance.updateTask);

        //DOM Event handler
        dom.submit.onclick = (ev) => {
            ev.preventDefault();
            console.log("submit clicked");
            let title = dom.title.value;
            let description = dom.description.value;
            if(title && description){
                todoModelInstance.add(title, description);
                dom.title.value = "";
                dom.description.value = "";
            } else {
                alert("Please enter title and description");
            }
        }

        dom.list.onclick = (ev) => {
            //ev.preventDefault();
            if (ev.target.classList.contains('btn-danger')) {
                let id = ev.target.closest('div').dataset.taskid;
                todoModelInstance.delete(id);
            } else if (ev.target.classList.contains('check')) {
                let id = ev.target.closest('div').dataset.taskid;
                todoModelInstance.check(id, ev.target.checked);
            }
        }
    }
}

//singleton pattern
export const controllerInstance = new TodoController();