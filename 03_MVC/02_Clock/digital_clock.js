
export default class DigitalClock{
    setClock(date){
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();

        document.querySelector(".digital-clock").innerHTML=
            `${this.#format(hours)}:${this.#format(minutes)}:${this.#format(seconds)}`
    }

    #format(value) {
        if (value < 10) {
            value = '0' + value
        }
        return value;
    }
}
