export default class Subject{
    //assoziativer Array
    observers = [];

    constructor() {
        //this.observers = [];
    }

    subscribe(event, observer,callback){
        if(!this.observers[event]){
            this.observers[event] = [];
        }
        this.observers[event].push({
            observer: observer,
            callback: callback
        })
    }

    unsubscribe(event,observer){
        let observersForEvent = this.observers[event];
        if(observersForEvent) {
            for (let i = 0; i < observersForEvent.length; i++) {
                if (observersForEvent[i].observer === observer) {
                    observersForEvent.splice(i, 1);
                    return;
                }
            }
        } else {
            throw new Error("ERROR: could not find desired event " + event);
        }
    }

    notify(event,param){
        let observersForEvent = this.observers[event];
        if(observersForEvent) {
            for(let observer of observersForEvent){
                observer.callback.call(observer.observer,param);
            }
        } else {
            throw new Error("ERROR: could not find desired event " + event);
        }

    }
}