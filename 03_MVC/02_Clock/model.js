import Subject from './subject.js';

class ClockModel extends Subject {
    start(){
        setInterval(()=>{
            this.notify("tick", new Date());
        },1000);
    }
}

export const modelInstance = new ClockModel();
