import Newsletter from "./newsletter.js";
import Person from "./person.js";

window.onload = function (){
    let newsletter = new Newsletter();

    let div = document.querySelector("#eventDiv");
    newsletter.subscribe("sendLetter",div, function(param){
       console.log(param);
       this.innerHTML += `<p>${param.message}</p>`;
    });

    let p1 = new Person("Hannes", "Schönböck");
    newsletter.subscribe("sendLetter",p1,p1.receivedMessage);


    newsletter.send("sendLetter","Erste Nachricht vom KWM Newsletter");

    newsletter.unsubscribe("sendLetter",p1);
    newsletter.send("sendLetter","Zweite Nachricht vom KWM Newsletter");
}