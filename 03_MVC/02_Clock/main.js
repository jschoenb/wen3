import DigitalClock from "./digital_clock.js";
import AnalogClock from "./analog_clock.js";
import {modelInstance} from "./model.js";

let digitalClock = new DigitalClock();
let analogClock = new AnalogClock();

modelInstance.subscribe("tick",digitalClock,digitalClock.setClock);
modelInstance.subscribe("tick",analogClock,analogClock.setClock);
modelInstance.start();