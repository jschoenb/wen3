export default class Newsletter {
    send(text, receiver) {
        let event = new CustomEvent("sendLetter", {
            detail: {
                message: text
            }
        });
        receiver.dispatchEvent(event)
    }
}