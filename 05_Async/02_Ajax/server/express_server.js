let express = require('express');
let bodyParser = require('body-parser');
let loki = require("lokijs");
let cors = require('cors')

let app = express();
app.use(bodyParser.json());
app.use(cors());

let shopping = null;
let db = null;
let lastId = 0;

init();

function init(){
    db = new loki('loki.json');
    db.loadDatabase({}, (err)=>{
        if (err) {
            console.log("error : " + err);
        } else {
            shopping = db.getCollection('shoppinglist');
            if(!shopping){
                console.log("created new collection");
                shopping = db.addCollection('shoppinglist');
                saveDatabase();
            }
            //set last id
            let max = shopping.max("id");
            if(max == -Infinity){
                max = 0;
            }
            lastId = max;
            console.log("Max: " + max);
        }
    });
}

function saveDatabase() {
    db.saveDatabase(function(err) {
        if (err) {
            console.log("error : " + err);
        }else {
            console.log("database saved.");
        }
    });
}

app.get('/', function (req, res) {
    let results = shopping.find();
    res.json(results);
});

app.get('/shopping', function (req, res) {
    let results = shopping.find();
    res.status(200).json(results);
});

app.get('/shopping/:id', function (req, res) {
    let param = req.params.id;
    console.log('Parameter: ' + param);
    let result = shopping.find({'id': { '$eq' : Number(param) }});
    if(result.length==0){
        res.status(400).json({"errorMsg:":"Could not find item with id"});
    } else {
        res.status(200).json(result[0]);
    }
});

app.post('/shopping', function (req, res) {
    console.log("trying to add new item");
    let jsonBody = req.body;
    console.log(jsonBody);
    if(jsonBody.title==undefined || jsonBody.number==undefined){
        res.status(400).json({"errorMsg:":"Title and number must be defined"});
    } else {
        jsonBody.id = (++lastId);
        shopping.insert(jsonBody);
        saveDatabase();
        res.status(200).json(jsonBody);
    }
});

app.put('/shopping/:id', function (req, res) {
    let param = req.params.id;
    let jsonBody = req.body;
    //check if item exists
    let result = shopping.find({'id': {'$eq': Number(param)}});
    if (result.length == 0) {
        res.status(400).json({"errorMsg:": "Could not find item with id"});
    } else {
        shopping.findAndUpdate({'id': {'$eq': Number(param)}}, (obj) => {
            obj.title = jsonBody.title;
            obj.number = jsonBody.number;
            saveDatabase();
            res.status(200).json(obj);
        });
    }
});

app.delete('/shopping/:id', function (req, res) {
    let param = req.params.id;
    let jsonBody = req.body;
    //check if item exists
    let result = shopping.find({'id': {'$eq': Number(param)}});
    if (result.length == 0) {
        res.status(400).json({"errorMsg:": "Could not find item with id"});
    } else {
        shopping.findAndRemove({'id': {'$eq': Number(param)}});
        saveDatabase();
        res.status(200).json({id:param});
    }
});

app.listen(3000, function () {
    console.log('App listening on port 3000!');
});
