class TodoView {
    #dom
    constructor() {
        this.#dom = {
            title : document.querySelector("#todo-title"),
            description : document.querySelector("#todo-description"),
            submit: document.querySelector("#todo-submit"),
            list: document.querySelector("#todo-list")
        }
    }

    get dom() {
        return this.#dom;
    }

    addTask(task){
        let html = this.#getHTML(task);
        this.#dom.list.insertAdjacentHTML("beforeend", html);
    }

    deleteTask(task){
        let elementToDelete = this.#dom.list.querySelector(`[data-taskid="${task.id}"]`);
        if(elementToDelete){
            elementToDelete.remove();
        }
    }

    updateTask(task){
        console.log(task);
        let elementToUpdate = this.#dom.list.querySelector(`[data-taskid="${task.id}"]`);
        console.log(elementToUpdate);
        if(elementToUpdate ){
            let cb = elementToUpdate.querySelector('.check');
            cb.checked = task.complete;
            elementToUpdate.classList.add('bg-success');
        }
    }

    //==========private methods ===================
    #isChecked(elem) {
        return elem.complete === true ? 'checked': '';
    }

    #getHTML(task){
        return `
            <div class="card" style="width: 18rem;">
              <div class="card-body" data-taskid="${task.id}">
                <h5 class="card-title">${task.title}</h5>
                <p class="card-text">${task.description}</p>
                <input class="check" type="checkbox" ${this.#isChecked(task)}>
                <button type="button" class="btn btn-danger">Delete</button>
              </div>
            </div>`
    }
}

export const viewInstance = new TodoView();