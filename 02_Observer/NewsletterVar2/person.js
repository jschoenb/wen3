export default class Person{
    #firstName;
    #lastName;
    constructor(firstName, lastName) {
        this.#firstName = firstName;
        this.#lastName = lastName;
    }

    receivedMessage(msg) {
        console.log(this);
        console.log(`${this.#firstName} ${this.#lastName} received the following message: ${msg.message}`);
    }
}