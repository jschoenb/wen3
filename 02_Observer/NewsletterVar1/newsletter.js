export default class Newsletter {
    observers = [];

    constructor() {
        //this.observers = [];
    }

    send(text) {
        this.notify({message:text});
    }

    subscribe(observer,callback){
        this.observers.push({
            observer: observer,
            callback: callback
        })
    }

    unsubscribe(observer){
        for(let i=0; i<this.observers.length; i++){
            if(this.observers[i].observer === observer){
                this.observers.splice(i,1);
                return;
            }
        }
    }

    notify(param){
        for(let observer of this.observers){
            observer.callback.call(observer.observer,param);
        }
    }
}