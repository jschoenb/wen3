import Subject from "./subject.js";
import Project from "./project.js";
import Employee from "./employee.js";


class Model extends Subject {
    constructor (){
        super();
        this.employees = new Map();
        this.projects = new Map();
        this.#init();
    }

    addProject(title,budget,desc,employees){
        if(title == undefined || budget == undefined || desc == undefined || employees == undefined){
            console.error("missing data");
            this.notify("error",{msg:"Bitte alle Felder ausfüllen"});
        } else {
            let project = new Project(title,budget,desc);
            for(let id of employees){
                let empl = this.employees.get(Number(id));
                if(empl){
                    empl.addProject(project);
                    project.addEmployee(empl);
                    this.notify("updateEmployee",empl);
                }
            }
            this.projects.set(project.id,project);
            this.notify("addProject",project);
        }
    }

    deleteProject(id){
    }

    #init(){
        let e1 = new Employee("Hans", "Huber")
        this.employees.set(e1.id,e1);
        let e2 = new Employee("Susi", "Musterfrau")
        this.employees.set(e2.id,e2);
        let e3 = new Employee("Max", "Mustermann")
        this.employees.set(e3.id,e3);
    }
}

export const model = new Model();